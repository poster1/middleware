<?php

namespace App\UseCase\Comment\List;

class CommentListRequest
{
    public ?int $postId;
    public ?int $authorId;
    public ?int $parentId;
    public ?int $page;
    public ?int $itemPerPage;
    public ?array $violations = null;
}
