<?php

namespace App\UseCase\Comment\List;

class CommentListResponse
{
    private ?array $violations = null;
    private ?array $comments = null;
    private ?int $page = null;
    private ?int $itemPerPage = null;

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function setViolations(?array $violations): void
    {
        $this->violations = $violations;
    }

    public function getComments(): ?array
    {
        return $this->comments;
    }

    public function setComments(?array $comments): void
    {
        $this->comments = $comments;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function setPage(?int $page): CommentListResponse
    {
        $this->page = $page;

        return $this;
    }

    public function getItemPerPage(): ?int
    {
        return $this->itemPerPage;
    }

    public function setItemPerPage(?int $itemPerPage): CommentListResponse
    {
        $this->itemPerPage = $itemPerPage;

        return $this;
    }
}
