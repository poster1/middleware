<?php

namespace App\UseCase\Comment\List;

use App\Domain\Comment\Exception\AuthorNotExistsException;
use App\Domain\Comment\Exception\ParentNotExistsException;
use App\Domain\Comment\Exception\PostNotExistsException;
use App\Domain\Comment\Interface\CommentListPresenterInterface;
use App\Domain\Comment\Repository\CommentRepositoryInterface;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class CommentListUseCase
{
    public function __construct(
        private readonly CommentRepositoryInterface $commentRepository,
        private readonly PostRepositoryInterface $postRepository,
        private readonly UserRepositoryInterface $userRepository,
    ) {
    }

    public function execute(
        CommentListRequest $commentListRequest,
        CommentListPresenterInterface $presenter,
    ): void {
        $commentListResponse = new CommentListResponse();
        $commentListResponse->setComments(null);
        $commentListResponse->setViolations(null);

        if ($commentListRequest->violations) {
            $commentListResponse->setViolations($commentListRequest->violations);
        }

        if (null === $commentListRequest->violations) {
            try {
                $comments = $this->getComments($commentListRequest);
                $commentListResponse->setComments($comments);
            } catch (AuthorNotExistsException $exception) {
                $commentListResponse->setViolations([
                    'author' => $exception->getMessage(),
                ]);
            } catch (PostNotExistsException $exception) {
                $commentListResponse->setViolations([
                    'post' => $exception->getMessage(),
                ]);
            }
        }

        $presenter->present($commentListResponse);
    }

    /**
     * @throws AuthorNotExistsException|PostNotExistsException|ParentNotExistsException
     */
    private function getComments(CommentListRequest $commentListRequest): array
    {
        $parentId = $commentListRequest->parentId ?? 0;
        $parent = $parentId ? $this->commentRepository->find($parentId) : null;
        if ($parentId && null === $parent) {
            throw ParentNotExistsException::withParentId($parentId);
        }

        $postId = $commentListRequest->postId ?? 0;
        $post = $postId ? $this->postRepository->find($postId) : null;
        if ($postId && null === $post) {
            throw PostNotExistsException::withPostId($commentListRequest->postId);
        }

        $authorId = $commentListRequest->authorId ?? 0;
        $author = $authorId ? $this->userRepository->find($commentListRequest->authorId) : null;
        if ($authorId && null === $author) {
            throw AuthorNotExistsException::withAuthorId($commentListRequest->authorId);
        }

        return $this->commentRepository->findByCommentListRequest($commentListRequest);
    }
}
