<?php

namespace App\UseCase\Comment\Add;

use App\Domain\Comment\Entity\Comment;
use App\Domain\Comment\Exception\AuthorNotExistsException;
use App\Domain\Comment\Exception\ParentNotExistsException;
use App\Domain\Comment\Exception\PostNotExistsException;
use App\Domain\Comment\Interface\CommentAddPresenterInterface;
use App\Domain\Comment\Repository\CommentRepositoryInterface;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class CommentAddUseCase
{
    public function __construct(
        private readonly CommentRepositoryInterface $commentRepository,
        private readonly PostRepositoryInterface $postRepository,
        private readonly UserRepositoryInterface $userRepository,
    ) {
    }

    public function execute(
        CommentAddRequest $commentAddRequest,
        CommentAddPresenterInterface $presenter,
    ): void {
        $commentAddResponse = new CommentAddResponse();
        $commentAddResponse->setComment(null);
        $commentAddResponse->setViolations(null);

        if ($commentAddRequest->violations) {
            $commentAddResponse->setViolations($commentAddRequest->violations);
        }

        if (null === $commentAddRequest->violations) {
            try {
                $comment = $this->addComment($commentAddRequest);
                $commentAddResponse->setComment($comment);
            } catch (ParentNotExistsException $exception) {
                $commentAddResponse->setViolations([
                    'parent' => $exception->getMessage(),
                ]);
            } catch (AuthorNotExistsException $exception) {
                $commentAddResponse->setViolations([
                    'author' => $exception->getMessage(),
                ]);
            } catch (PostNotExistsException $exception) {
                $commentAddResponse->setViolations([
                    'post' => $exception->getMessage(),
                ]);
            }
        }

        $presenter->present($commentAddResponse);
    }

    /**
     * @throws AuthorNotExistsException
     * @throws PostNotExistsException
     * @throws ParentNotExistsException
     */
    private function addComment(CommentAddRequest $commentAddRequest): Comment
    {
        $parentId = $commentAddRequest->parentId ?? 0;
        $parent = $parentId ? $this->commentRepository->find($parentId) : null;
        if ($parentId && null === $parent) {
            throw ParentNotExistsException::withParentId($parentId);
        }

        $postId = $commentAddRequest->postId ?? 0;
        $post = $postId ? $this->postRepository->find($postId) : null;
        if (null === $post) {
            throw PostNotExistsException::withPostId($postId);
        }

        $authorId = $commentAddRequest->authorId ?? 0;
        $author = $authorId ? $this->userRepository->find($authorId) : null;
        if (null === $author) {
            throw AuthorNotExistsException::withAuthorId($authorId);
        }

        $comment = new Comment();
        $comment->setContent($commentAddRequest->content);
        $comment->setPost($post);
        $comment->setAuthor($author);
        $comment->setParent($parent);

        $this->commentRepository->add($comment, true);

        return $comment;
    }
}
