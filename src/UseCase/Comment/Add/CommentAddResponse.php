<?php

namespace App\UseCase\Comment\Add;

use App\Domain\Comment\Entity\Comment;

class CommentAddResponse
{
    private ?array $violations;
    private ?Comment $comment;

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function setViolations(?array $violations): void
    {
        $this->violations = $violations;
    }

    public function getComment(): ?Comment
    {
        return $this->comment;
    }

    public function setComment(?Comment $comment): CommentAddResponse
    {
        $this->comment = $comment;

        return $this;
    }
}
