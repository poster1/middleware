<?php

namespace App\UseCase\Comment\Add;

class CommentAddRequest
{
    public ?bool $isPosted = null;
    public ?int $id;
    public ?string $content;
    public ?int $postId;
    public ?int $authorId;
    public ?int $parentId;
    public ?array $violations = null;
}
