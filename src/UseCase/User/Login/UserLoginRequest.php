<?php

namespace App\UseCase\User\Login;

class UserLoginRequest
{
    public ?bool $isPosted = null;


    public ?string $email;

    public ?string $password;

    public ?array $violations = null;
}
