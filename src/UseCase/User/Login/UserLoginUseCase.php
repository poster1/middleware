<?php

namespace App\UseCase\User\Login;

use App\Domain\User\Entity\User;
use App\Domain\User\Exception\InvalidCredentialsException;
use App\Domain\User\Interface\UserLoginPresenterInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class UserLoginUseCase
{
    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
    ) {
    }

    public function execute(
        UserLoginRequest $loginRequest,
        UserLoginPresenterInterface $presenter
    ): void {
        $loginResponse = new UserLoginResponse();
        $loginResponse->setToken(null);
        $loginResponse->setUser(null);
        $loginResponse->setViolations(null);
        if ($loginRequest->violations) {
            $loginResponse->setViolations($loginRequest->violations);
        }

        if ($loginRequest->isPosted && (null === $loginRequest->violations)) {
            try {
                $user = $this->saveUser($loginRequest);
                $loginResponse->setToken($user->getToken());
                $loginResponse->setUser($user);
            } catch (InvalidCredentialsException $exception) {
                $loginResponse->setViolations([['email' => $exception->getMessage()]]);
            }
        }

        $presenter->present($loginResponse);
    }

    /**
     * @throws InvalidCredentialsException
     */
    private function saveUser(UserLoginRequest $loginRequest): User
    {
        if (null === $this->userRepository->findOneByEmailAndPassword($loginRequest->email, $loginRequest->password)) {
            throw InvalidCredentialsException::withEmail($loginRequest->email);
        }

        return $this->userRepository->login($loginRequest->email, $loginRequest->password);
    }
}
