<?php

namespace App\UseCase\User\Register;

use App\Domain\User\Entity\User;
use App\Domain\User\Exception\UserAlreadyExistsException;
use App\Domain\User\Interface\RegisterUserPresenterInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class RegisterUserUseCase
{
    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
    ) {
    }

    public function execute(
        RegisterUserRequest $registerRequest,
        RegisterUserPresenterInterface $presenter
    ): void {
        $registerResponse = new RegisterUserResponse();
        $registerResponse->setUser(null);
        $registerResponse->setViolations(null);
        if ($registerRequest->violations) {
            $registerResponse->setViolations($registerRequest->violations);
        }

        if ($registerRequest->isPosted && null === $registerRequest->violations) {
            try {
                $user = $this->saveUser($registerRequest);
                $registerResponse->setUser($user);
            } catch (UserAlreadyExistsException $exception) {
                $registerResponse->setViolations([['email' => $exception->getMessage()]]);
            }
        }

        $presenter->present($registerResponse);
    }

    /**
     * @throws UserAlreadyExistsException
     */
    private function saveUser(RegisterUserRequest $registerRequest): User
    {
        if (null !== $this->userRepository->findOneByEmail($registerRequest->email)) {
            throw UserAlreadyExistsException::withEmail($registerRequest->email);
        }

        $user = new User();
        $user->setFirstName($registerRequest->firstName);
        $user->setLastName($registerRequest->lastName);
        $user->setEmail($registerRequest->email);
        $user->setPassword($registerRequest->password);

        $this->userRepository->add($user, true);

        return $user;
    }
}
