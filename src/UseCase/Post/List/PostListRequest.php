<?php

namespace App\UseCase\Post\List;

class PostListRequest
{
    public ?int $authorId;

    public ?int $itemPerPage;

    public ?int $page;
    public ?array $violations = null;
}
