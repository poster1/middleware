<?php

namespace App\UseCase\Post\List;

use App\Domain\Post\Exception\PageNotExistsException;
use App\Domain\Post\Interface\PostListPresenterInterface;
use App\Domain\Post\Repository\PostRepositoryInterface;

class PostListUseCase
{
    public function __construct(
        private readonly PostRepositoryInterface $postRepository,
    ) {
    }

    public function execute(
        PostListRequest $postListRequest,
        PostListPresenterInterface $presenter,
    ): void {
        $postListResponse = new PostListResponse();
        $postListResponse->setPosts(null);
        $postListResponse->setViolations(null);

        if ($postListRequest->violations) {
            $postListResponse->setViolations($postListRequest->violations);
        }

        if (null === $postListRequest->violations) {
            try {
                $posts = $this->getPosts($postListRequest);
                $postListResponse->setPosts($posts);
            } catch (PageNotExistsException $exception) {
                $postListResponse->setViolations([
                    'page' => $exception->getMessage(),
                ]);
            }
        }

        $presenter->present($postListResponse);
    }

    /**
     * @throws PageNotExistsException
     */
    private function getPosts(PostListRequest $postListRequest): array
    {
        $page = $postListRequest->page;
        $itemPerPage = $postListRequest->itemPerPage;

        if ($page && $itemPerPage) {
            $total = $this->postRepository->countAll();
            $totalPages = ceil($total / $itemPerPage);
            if ($page <= 0 || $page > $totalPages) {
                throw new PageNotExistsException();
            }
        }

        return $this->postRepository->findByPostListRequest($postListRequest);
    }
}
