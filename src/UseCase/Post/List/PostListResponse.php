<?php

namespace App\UseCase\Post\List;

class PostListResponse
{
    private ?array $violations = null;
    private ?array $posts = null;
    private ?int $page = null;
    private ?int $itemPerPage = null;

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function setViolations(?array $violations): void
    {
        $this->violations = $violations;
    }

    public function getPosts(): ?array
    {
        return $this->posts;
    }

    public function setPosts(?array $posts): void
    {
        $this->posts = $posts;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function setPage(?int $page): PostListResponse
    {
        $this->page = $page;

        return $this;
    }

    public function getItemPerPage(): ?int
    {
        return $this->itemPerPage;
    }

    public function setItemPerPage(?int $itemPerPage): PostListResponse
    {
        $this->itemPerPage = $itemPerPage;

        return $this;
    }
}
