<?php

namespace App\UseCase\Post\Add;

class PostAddRequest
{
    public ?bool $isPosted = null;
    public ?int $id;
    public ?string $content;
    public ?int $authorId;
    public ?array $violations = null;
}
