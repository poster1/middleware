<?php

namespace App\UseCase\Post\Add;

use App\Domain\Post\Entity\Post;

class PostAddResponse
{
    private ?array $violations;
    private ?Post $post;

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function setViolations(?array $violations): void
    {
        $this->violations = $violations;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): PostAddResponse
    {
        $this->post = $post;

        return $this;
    }
}
