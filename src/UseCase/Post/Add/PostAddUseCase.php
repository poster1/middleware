<?php

namespace App\UseCase\Post\Add;

use App\Domain\Post\Entity\Post;
use App\Domain\Post\Exception\AuthorNotExistsException;
use App\Domain\Post\Interface\PostAddPresenterInterface;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class PostAddUseCase
{
    public function __construct(
        private readonly PostRepositoryInterface $postRepository,
        private readonly UserRepositoryInterface $userRepository,
    ) {
    }

    public function execute(
        PostAddRequest $postAddRequest,
        PostAddPresenterInterface $presenter,
    ): void {
        $postAddResponse = new PostAddResponse();
        $postAddResponse->setPost(null);
        $postAddResponse->setViolations(null);

        if ($postAddRequest->violations) {
            $postAddResponse->setViolations($postAddRequest->violations);
        }

        if (null === $postAddRequest->violations) {
            try {
                $post = $this->addPost($postAddRequest);
                $postAddResponse->setPost($post);
            } catch (AuthorNotExistsException $exception) {
                $postAddResponse->setViolations([
                    'author' => $exception->getMessage(),
                ]);
            }
        }

        $presenter->present($postAddResponse);
    }

    /**
     * @throws AuthorNotExistsException
     */
    private function addPost(PostAddRequest $postAddRequest): Post
    {
        $author = $this->userRepository->find($postAddRequest->authorId);
        if (null === $author) {
            throw AuthorNotExistsException::withAuthorId($postAddRequest->authorId);
        }

        $post = new Post();
        $post->setContent($postAddRequest->content);
        $post->setAuthor($author);
        $this->postRepository->add($post, true);

        return $post;
    }
}
