<?php

namespace App\UseCase\Post\Show;

use App\Domain\Post\Entity\Post;
use App\Domain\Post\Exception\PostNotExistsException;
use App\Domain\Post\Interface\PostShowPresenterInterface;
use App\Domain\Post\Repository\PostRepositoryInterface;

class PostShowUseCase
{
    public function __construct(
        private readonly PostRepositoryInterface $postRepository,
    ) {
    }

    public function execute(
        PostShowRequest $postShowRequest,
        PostShowPresenterInterface $presenter,
    ): void {
        $postShowResponse = new PostShowResponse();
        $postShowResponse->setPost(null);
        $postShowResponse->setViolations(null);

        if ($postShowRequest->violations) {
            $postShowResponse->setViolations($postShowRequest->violations);
        }

        if (null === $postShowRequest->violations) {
            try {
                $post = $this->getPost($postShowRequest);
                $postShowResponse->setPost($post);
            } catch (PostNotExistsException $exception) {
                $postShowResponse->setViolations([
                    'post' => $exception->getMessage(),
                ]);
            }
        }

        $presenter->present($postShowResponse);
    }

    /**
     * @throws PostNotExistsException
     */
    private function getPost(PostShowRequest $postShowRequest): ?Post
    {
        $post = $this->postRepository->find($postShowRequest->id);

        if (null === $post) {
            throw new PostNotExistsException();
        }

        return $post;
    }
}
