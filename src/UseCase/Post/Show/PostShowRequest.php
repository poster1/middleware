<?php

namespace App\UseCase\Post\Show;

class PostShowRequest
{
    public ?int $id;
    public ?array $violations = null;
}
