<?php

namespace App\UseCase\Post\Show;

use App\Domain\Post\Entity\Post;

class PostShowResponse
{
    private ?array $violations = null;
    private ?Post $post = null;

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function setViolations(?array $violations): void
    {
        $this->violations = $violations;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): void
    {
        $this->post = $post;
    }
}
