<?php

namespace App\UseCase\Rating\Add;

use App\Domain\Comment\Repository\CommentRepositoryInterface;
use App\Domain\Rating\Entity\Rating;
use App\Domain\Rating\Exception\AuthorNotExistsException;
use App\Domain\Rating\Exception\CommentNotExistsException;
use App\Domain\Rating\Exception\InvalidValueException;
use App\Domain\Rating\Interface\RatingAddPresenterInterface;
use App\Domain\Rating\Repository\RatingRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class RatingAddUseCase
{
    public function __construct(
        private readonly RatingRepositoryInterface $ratingRepository,
        private readonly CommentRepositoryInterface $commentRepository,
        private readonly UserRepositoryInterface $userRepository
    ) {
    }

    public function execute(
        RatingAddRequest $ratingAddRequest,
        RatingAddPresenterInterface $presenter,
    ): void {
        $ratingAddResponse = new RatingAddResponse();
        $ratingAddResponse->setRating(null);
        $ratingAddResponse->setViolations(null);

        if ($ratingAddRequest->violations) {
            $ratingAddResponse->setViolations($ratingAddRequest->violations);
        }

        if (null === $ratingAddRequest->violations) {
            try {
                $rating = $this->addRating($ratingAddRequest);
                $ratingAddResponse->setRating($rating);
            } catch (AuthorNotExistsException $exception) {
                $ratingAddResponse->setViolations([
                    'author' => $exception->getMessage(),
                ]);
            } catch (CommentNotExistsException $exception) {
                $ratingAddResponse->setViolations([
                    'comment' => $exception->getMessage(),
                ]);
            } catch (InvalidValueException $exception) {
                $ratingAddResponse->setViolations([
                    'value' => $exception->getMessage(),
                ]);
            }
        }

        $presenter->present($ratingAddResponse);
    }

    /**
     * @throws AuthorNotExistsException
     * @throws CommentNotExistsException
     * @throws InvalidValueException
     */
    private function addRating(RatingAddRequest $ratingAddRequest): Rating
    {
        $comment = $this->commentRepository->find($ratingAddRequest->commentId);
        if (null === $comment) {
            throw CommentNotExistsException::withCommentId($ratingAddRequest->commentId);
        }

        $author = $this->userRepository->find($ratingAddRequest->authorId);
        if (null === $author) {
            throw AuthorNotExistsException::withAuthorId($ratingAddRequest->authorId);
        }

        if ($ratingAddRequest->value < 0 || $ratingAddRequest->value > 5) {
            throw InvalidValueException::withValue($ratingAddRequest->value);
        }

        $rating = new Rating();
        $rating->setComment($comment);
        $rating->setAuthor($author);
        $rating->setAuthor($author);
        $rating->setValue($ratingAddRequest->value);
        $this->ratingRepository->add($rating, true);

        return $rating;
    }
}
