<?php

namespace App\UseCase\Rating\Add;

class RatingAddRequest
{
    public ?bool $isPosted = null;
    public ?int $id;
    public ?int $value;
    public ?int $commentId;
    public ?int $authorId;
    public ?array $violations = null;
}
