<?php

namespace App\UseCase\Rating\Add;

use App\Domain\Rating\Entity\Rating;

class RatingAddResponse
{
    private ?array $violations;
    private ?Rating $rating;

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function setViolations(?array $violations): void
    {
        $this->violations = $violations;
    }

    public function getRating(): ?Rating
    {
        return $this->rating;
    }

    public function setRating(?Rating $rating): void
    {
        $this->rating = $rating;
    }
}
