<?php

namespace App\UseCase\Rating\List;

class RatingListRequest
{
    public ?int $commentId;
    public ?int $authorId;
    public ?int $page;
    public ?int $itemPerPage;
    public ?array $violations = null;
}
