<?php

namespace App\UseCase\Rating\List;

class RatingListResponse
{
    private ?array $violations = null;
    private ?array $ratings = null;
    private ?int $page = null;
    private ?int $itemPerPage = null;

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function setViolations(?array $violations): void
    {
        $this->violations = $violations;
    }

    public function getRatings(): ?array
    {
        return $this->ratings;
    }

    public function setRatings(?array $ratings): void
    {
        $this->ratings = $ratings;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function setPage(?int $page): RatingListResponse
    {
        $this->page = $page;

        return $this;
    }

    public function getItemPerPage(): ?int
    {
        return $this->itemPerPage;
    }

    public function setItemPerPage(?int $itemPerPage): RatingListResponse
    {
        $this->itemPerPage = $itemPerPage;

        return $this;
    }
}
