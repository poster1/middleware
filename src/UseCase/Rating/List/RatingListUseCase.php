<?php

namespace App\UseCase\Rating\List;

use App\Domain\Comment\Repository\CommentRepositoryInterface;
use App\Domain\Rating\Exception\CommentNotExistsException;
use App\Domain\Rating\Interface\RatingListPresenterInterface;
use App\Domain\Rating\Repository\RatingRepositoryInterface;

class RatingListUseCase
{
    public function __construct(
        private readonly RatingRepositoryInterface $ratingRepository,
        private readonly CommentRepositoryInterface $commentRepository,
    ) {
    }

    public function execute(
        RatingListRequest $ratingListRequest,
        RatingListPresenterInterface $presenter,
    ): void {
        $ratingListResponse = new RatingListResponse();
        $ratingListResponse->setRatings(null);
        $ratingListResponse->setViolations(null);

        if ($ratingListRequest->violations) {
            $ratingListResponse->setViolations($ratingListRequest->violations);
        }

        if (null === $ratingListRequest->violations) {
            try {
                $ratings = $this->getRatings($ratingListRequest);
                $ratingListResponse->setRatings($ratings);
            } catch (CommentNotExistsException $exception) {
                $ratingListResponse->setViolations([
                    'comment' => $exception->getMessage(),
                ]);
            }
        }

        $presenter->present($ratingListResponse);
    }

    /**
     * @throws CommentNotExistsException
     */
    private function getRatings(RatingListRequest $ratingListRequest): array
    {
        if ($ratingListRequest->commentId && null === $this->commentRepository->find($ratingListRequest->commentId)) {
            throw CommentNotExistsException::withCommentId($ratingListRequest->commentId);
        }

        return $this->ratingRepository->findByRatingListRequest($ratingListRequest);
    }
}
