<?php

namespace App\Domain\Comment\Entity;

use App\Domain\Post\Entity\Post;
use App\Domain\Rating\Entity\Rating;
use App\Domain\User\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Comment
{
    protected int $id;
    protected string $content;

    protected Post $post;
    protected User $author;
    protected ?Comment $parent = null;
    protected Collection $children;
    protected Collection $ratings;

    public function __construct(
    ) {
        $this->children = new ArrayCollection();
        $this->ratings = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function setPost(Post $post): Comment
    {
        $this->post = $post;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author): Comment
    {
        $this->author = $author;

        return $this;
    }

    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function setRatings(Collection $ratings): Comment
    {
        $this->ratings = $ratings;

        return $this;
    }

    public function addRating(Rating $rating): Comment
    {
        if (!$this->ratings->contains($rating)) {
            $this->ratings->add($rating);
            $rating->setComment($this);
        }

        return $this;
    }

    public function removeRating(Rating $rating): Comment
    {
        if ($this->ratings->contains($rating)) {
            $this->ratings->removeElement($rating);
        }

        return $this;
    }

    public function getParent(): ?Comment
    {
        return $this->parent;
    }

    public function setParent(?Comment $parent): Comment
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function setChildren(Collection $children): Comment
    {
        $this->children = $children;

        return $this;
    }

    public function addChild(Comment $child): Comment
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Comment $child): Comment
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            $child->setParent(null);
        }

        return $this;
    }

    public static function toArray(?Comment $comment, bool $nested = false): ?array
    {
        if (!$comment) {
            return null;
        }

        $array = [
            'id' => $comment->getId(),
            'content' => $comment->getContent(),
            'author' => User::toArray($comment->getAuthor(), true),
        ];

        if (!$nested) {
            $array = [
                ...$array,
                'post' => Post::toArray($comment->getPost(), true),
                'parent' => Comment::toArray($comment->getParent(), true),
                'children' => array_map(
                    fn (Comment $child) => Comment::toArray($child, true),
                    $comment->getChildren()->toArray()
                ),
                'ratings' => array_map(
                    fn (Rating $rating) => Rating::toArray($rating, true),
                    $comment->getRatings()->toArray()
                ),
            ];
        }

        return $array;
    }
}
