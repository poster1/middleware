<?php

namespace App\Domain\Comment\Interface;

use App\UseCase\Comment\List\CommentListResponse;

interface CommentListPresenterInterface
{
    public function present(CommentListResponse $commentListResponse): void;
}
