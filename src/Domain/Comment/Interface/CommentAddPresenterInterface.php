<?php

namespace App\Domain\Comment\Interface;

use App\UseCase\Comment\Add\CommentAddResponse;

interface CommentAddPresenterInterface
{
    public function present(CommentAddResponse $commentAddResponse): void;
}
