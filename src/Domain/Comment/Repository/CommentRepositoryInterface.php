<?php

namespace App\Domain\Comment\Repository;

use App\Domain\Comment\Entity\Comment;
use App\Domain\Post\Entity\Post;
use App\UseCase\Comment\List\CommentListRequest;

interface CommentRepositoryInterface
{
    public function add(Comment $comment): void;

    /**
     * @return Comment[]
     */
    public function findByPostId(int $postId): array;

    public function findByAuthorId(int $authorId): array;

    /**
     * @return Post[]
     */
    public function findByParentId(int $parentId): array;

    /**
     * @return Comment[]
     */
    public function findByCommentListRequest(CommentListRequest $commentListRequest): array;
}
