<?php

namespace App\Domain\Comment\Exception;

class ParentNotExistsException extends \Exception
{
    public static function withParentId(int $parentId): self
    {
        return new self(\sprintf('parent with parent id %s does not exists', $parentId));
    }
}
