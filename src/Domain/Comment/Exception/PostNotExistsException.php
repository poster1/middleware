<?php

namespace App\Domain\Comment\Exception;

class PostNotExistsException extends \Exception
{
    public static function withPostId(int $postId): self
    {
        return new self(\sprintf('post with post id %s does not exists', $postId));
    }
}
