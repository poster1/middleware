<?php

namespace App\Domain\User\Exception;

class InvalidCredentialsException extends \Exception
{
    public static function withEmail(string $email): self
    {
        return new self(\sprintf('user with email %s is does not exists or password is incorrect', $email));
    }
}
