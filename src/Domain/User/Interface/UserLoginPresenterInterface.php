<?php

namespace App\Domain\User\Interface;

use App\UseCase\User\Login\UserLoginResponse;

interface UserLoginPresenterInterface
{
    public function present(UserLoginResponse $userLoginResponse);
}
