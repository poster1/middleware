<?php

namespace App\Domain\User\Interface;

use App\UseCase\User\Register\RegisterUserResponse;

interface RegisterUserPresenterInterface
{
    public function present(RegisterUserResponse $response);
}
