<?php

namespace App\Domain\User\Entity;

use App\Domain\Comment\Entity\Comment;
use App\Domain\Post\Entity\Post;
use App\Domain\Rating\Entity\Rating;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLES = [
        self::ROLE_USER,
        self::ROLE_ADMIN,
    ];

    protected int $id;
    protected string $firstName;
    protected string $lastName;
    protected string $email;
    protected string $password;
    protected ?string $token = null;

    protected array $roles = [self::ROLE_USER];

    protected Collection $comments;

    protected Collection $posts;

    protected Collection $ratings;

    public function __construct(
    ) {
        $this->comments = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->ratings = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): User
    {
        $this->id = $id;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): User
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): User
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFullName(): string
    {
        return "$this->firstName $this->lastName";
    }

    public function getInitials(): string
    {
        return substr($this->firstName, 0, 1).substr($this->lastName, 0, 1);
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): User
    {
        $this->token = $token;

        return $this;
    }

    public function getRoles(): array
    {
        if ($this->getToken()) {
            return array_unique([...$this->roles, self::ROLE_ADMIN]);
        }

        return array_unique([...$this->roles, self::ROLE_USER]);
    }

    public function eraseCredentials(): string
    {
        return $this->getFullName();
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function setComments(array $comments): User
    {
        $this->comments = new ArrayCollection();
        foreach ($comments as $comment) {
            $this->addComment($comment);
        }

        return $this;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setAuthor($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
        }

        return $this;
    }

    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function setPosts(array $posts): User
    {
        $this->posts = new ArrayCollection();
        foreach ($posts as $post) {
            $this->addPost($post);
        }

        return $this;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts->add($post);
            $post->setAuthor($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
        }

        return $this;
    }

    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function setRatings(array $ratings): User
    {
        $this->ratings = new ArrayCollection();
        foreach ($ratings as $rating) {
            $this->addRating($rating);
        }

        return $this;
    }

    public function addRating(Rating $rating): self
    {
        if (!$this->ratings->contains($rating)) {
            $this->ratings->add($rating);
            $rating->setAuthor($this);
        }

        return $this;
    }

    public function removeRating(Rating $rating): self
    {
        if ($this->ratings->contains($rating)) {
            $this->ratings->removeElement($rating);
        }

        return $this;
    }

    public static function toArray(?User $user, bool $nested = false): ?array
    {
        if (!$user) {
            return null;
        }

        $array = [
            'id' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'fullName' => $user->getFullName(),
            'initials' => $user->getInitials(),
            'email' => $user->getEmail(),
            'roles' => $user->getRoles(),
        ];

        if (!$nested) {
            $array = [
                ...$array,
                'comments' => array_map(
                    fn (Comment $comment) => Comment::toArray($comment),
                    $user->getComments()->toArray()
                ),
                'posts' => array_map(fn (Post $post) => Post::toArray($post), $user->getPosts()->toArray()),
                'ratings' => array_map(fn (Rating $rating) => Rating::toArray($rating), $user->getRatings()->toArray()),
            ];
        }

        return $array;
    }
}
