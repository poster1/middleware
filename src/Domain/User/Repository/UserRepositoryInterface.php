<?php

namespace App\Domain\User\Repository;

use App\Domain\User\Entity\User;

interface UserRepositoryInterface
{
    public function add(User $user, bool $flush = false): void;
    public function remove(User $user, bool $flush = false): void;
}
