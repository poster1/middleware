<?php

namespace App\Domain\Rating\Interface;

use App\UseCase\Rating\Add\RatingAddResponse;

interface RatingAddPresenterInterface
{
    public function present(RatingAddResponse $ratingAddResponse): void;
}
