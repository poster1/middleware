<?php

namespace App\Domain\Rating\Interface;

use App\UseCase\Rating\List\RatingListResponse;

interface RatingListPresenterInterface
{
    public function present(RatingListResponse $ratingListResponse): void;
}
