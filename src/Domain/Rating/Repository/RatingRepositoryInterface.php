<?php

namespace App\Domain\Rating\Repository;

use App\Domain\Rating\Entity\Rating;
use App\UseCase\Rating\List\RatingListRequest;

interface RatingRepositoryInterface
{
    public function add(Rating $rating, bool $flush = false): void;

    public function remove(Rating $rating, bool $flush = false): void;

    /**
     * @return Rating[]
     */
    public function findByRatingListRequest(RatingListRequest $ratingListRequest): array;
}
