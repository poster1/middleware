<?php

namespace App\Domain\Rating\Exception;

class InvalidValueException extends \Exception
{
    public static function withValue(int $value): self
    {
        return new self(\sprintf('value %s is not between 0 and 5', $value));
    }
}
