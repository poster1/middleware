<?php

namespace App\Domain\Rating\Exception;

class CommentNotExistsException extends \Exception
{
    public static function withCommentId(int $commentId): self
    {
        return new self(\sprintf('comment with comment id %s does not exists', $commentId));
    }
}
