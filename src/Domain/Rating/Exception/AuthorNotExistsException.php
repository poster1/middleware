<?php

namespace App\Domain\Rating\Exception;

class AuthorNotExistsException extends \Exception
{
    public static function withAuthorId(int $authorId): self
    {
        return new self(\sprintf('author with author id %s does not exists', $authorId));
    }
}
