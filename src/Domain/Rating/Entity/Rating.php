<?php

namespace App\Domain\Rating\Entity;

use App\Domain\Comment\Entity\Comment;
use App\Domain\User\Entity\User;

class Rating
{
    protected int $id;
    protected int $value;
    protected Comment $comment;
    protected User $author;

    public function __construct(
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Rating
    {
        $this->id = $id;

        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): Rating
    {
        $this->value = $value;

        return $this;
    }

    public function getComment(): Comment
    {
        return $this->comment;
    }

    public function setComment(Comment $comment): Rating
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author): Rating
    {
        $this->author = $author;

        return $this;
    }

    public static function toArray(?Rating $rating, bool $nested = false): ?array
    {
        if (!$rating) {
            return null;
        }

        $array = [
            'id' => $rating->getId(),
            'value' => $rating->getValue(),
            'author' => User::toArray($rating->getAuthor(), true),
        ];

        if (!$nested) {
            $array = [
                ...$array,
                'comment' => Comment::toArray($rating->getComment(), true),
            ];
        }

        return $array;
    }
}
