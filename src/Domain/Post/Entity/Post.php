<?php

namespace App\Domain\Post\Entity;

use App\Domain\Comment\Entity\Comment;
use App\Domain\User\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Post
{
    protected int $id;
    protected string $content;
    protected User $author;
    protected Collection $comments;

    public function __construct(
    ) {
        $this->comments = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author): Post
    {
        $this->author = $author;

        return $this;
    }

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function setComments(Collection $comments): Post
    {
        $this->comments = $comments;

        return $this;
    }

    public function addComment(Comment $comment): Post
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setPost($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): Post
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
        }

        return $this;
    }

    public static function toArray(?Post $post, bool $nested = false): ?array
    {
        if (!$post) {
            return null;
        }

        $array = [
            'id' => $post->getId(),
            'content' => $post->getContent(),
            'author' => User::toArray($post->getAuthor(), true),
        ];

        if (!$nested) {
            $array = [
                ...$array,
                'comments' => array_map(
                    fn (Comment $comment) => Comment::toArray($comment, true),
                    $post->getComments()->toArray()
                ),
            ];
        }

        return $array;
    }
}
