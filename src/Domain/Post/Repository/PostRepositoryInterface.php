<?php

namespace App\Domain\Post\Repository;

use App\Domain\Post\Entity\Post;
use App\UseCase\Post\List\PostListRequest;

interface PostRepositoryInterface
{
    public function add(Post $post, bool $flush = false): void;

    public function remove(Post $post, bool $flush = false): void;

    /**
     * @return Post[]
     */
    public function findByAuthorId(int $authorId): array;

    public function findByPage(int $page, int $itemPerPage): array;

    public function countAll(): int;

    /**
     * @return Post[]
     */
    public function findByPostListRequest(PostListRequest $postListRequest): array;
}
