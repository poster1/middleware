<?php

namespace App\Domain\Post\Interface;

use App\UseCase\Post\List\PostListResponse;

interface PostListPresenterInterface
{
    public function present(PostListResponse $postListResponse): void;
}
