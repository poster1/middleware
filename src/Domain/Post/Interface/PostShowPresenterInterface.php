<?php

namespace App\Domain\Post\Interface;

use App\UseCase\Post\Show\PostShowResponse;

interface PostShowPresenterInterface
{
    public function present(PostShowResponse $postShowResponse): void;
}
