<?php

namespace App\Domain\Post\Interface;

use App\UseCase\Post\Add\PostAddResponse;

interface PostAddPresenterInterface
{
    public function present(PostAddResponse $postAddResponse): void;
}
