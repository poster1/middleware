<?php

namespace App\Domain\Post\Exception;

class PostNotExistsException extends \Exception
{
    public static function withPost(int $id): self
    {
        return new self(\sprintf('post %s does not exists', $id));
    }
}
