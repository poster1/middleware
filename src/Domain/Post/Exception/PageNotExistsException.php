<?php

namespace App\Domain\Post\Exception;

class PageNotExistsException extends \Exception
{
    public static function withPage(int $page, int $itemPerPage): self
    {
        return new self(\sprintf('page %s does not exists for %s', $page, $itemPerPage));
    }
}
