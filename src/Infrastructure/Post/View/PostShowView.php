<?php

namespace App\Infrastructure\Post\View;

use App\Domain\Post\Entity\Post;
use App\Infrastructure\Post\ViewModel\PostShowViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class PostShowView
{
    public function generate(
        PostShowViewModel $postShowViewModel
    ): JsonResponse {
        if ($postShowViewModel->violations) {
            return new JsonResponse(['errors' => $postShowViewModel->violations], 400);
        }

        return new JsonResponse(
            [
                'data' => Post::toArray($postShowViewModel->post),
            ]
        );
    }
}
