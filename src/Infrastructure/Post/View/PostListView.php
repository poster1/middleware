<?php

namespace App\Infrastructure\Post\View;

use App\Domain\Post\Entity\Post;
use App\Infrastructure\Post\Factory\PostToArrayFactory;
use App\Infrastructure\Post\ViewModel\PostListViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class PostListView
{
    public function generate(
        PostListViewModel $postListViewModel
    ): JsonResponse {
        if ($postListViewModel->violations) {
            return new JsonResponse(['errors' => $postListViewModel->violations], 400);
        }

        return new JsonResponse(
            [
                'data' => array_map(fn (Post $post) => Post::toArray($post), $postListViewModel->posts),
                'page' => $postListViewModel->page,
                'itemPerPage' => $postListViewModel->itemPerPage,
            ]
        );
    }
}
