<?php

namespace App\Infrastructure\Post\View;

use App\Domain\Post\Entity\Post;
use App\Infrastructure\Post\ViewModel\PostAddViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class PostAddView
{
    public function generate(
        PostAddViewModel $postAddViewModel
    ): JsonResponse {
        if ($postAddViewModel->violations) {
            return new JsonResponse(['errors' => $postAddViewModel->violations], 400);
        }

        return new JsonResponse(
            [
                'data' => Post::toArray($postAddViewModel->post),
            ]
        );
    }
}
