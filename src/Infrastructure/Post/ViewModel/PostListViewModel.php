<?php

namespace App\Infrastructure\Post\ViewModel;

class PostListViewModel
{
    public ?array $posts;
    public ?int $page;
    public ?int $itemPerPage;

    public ?array $violations;
}
