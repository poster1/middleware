<?php

namespace App\Infrastructure\Post\ViewModel;

use App\Domain\Post\Entity\Post;

class PostShowViewModel
{
    public ?Post $post;

    public ?array $violations;
}
