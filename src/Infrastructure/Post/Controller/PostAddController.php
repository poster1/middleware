<?php

namespace App\Infrastructure\Post\Controller;

use App\Infrastructure\Post\Presenter\PostAddPresenter;
use App\Infrastructure\Post\View\PostAddView;
use App\UseCase\Post\Add\PostAddRequest;
use App\UseCase\Post\Add\PostAddUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/post', name: 'post_add', methods: ['POST'])]
#[ParamConverter('postAddRequest', converter: 'post_add_converter')]
class PostAddController extends AbstractController
{
    public function __construct(
        private readonly PostAddView $postAddView,
        private readonly PostAddUseCase $postAddUseCase,
        private readonly PostAddPresenter $presenter
    ) {
    }

    public function __invoke(PostAddRequest $postAddRequest): JsonResponse
    {
        $this->postAddUseCase->execute($postAddRequest, $this->presenter);

        return $this->postAddView->generate($this->presenter->viewModel());
    }
}
