<?php

namespace App\Infrastructure\Post\Controller;

use App\Infrastructure\Post\Presenter\PostListPresenter;
use App\Infrastructure\Post\View\PostListView;
use App\UseCase\Post\List\PostListRequest;
use App\UseCase\Post\List\PostListUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/post', name: 'post_list', methods: ['GET'])]
#[ParamConverter('postListRequest', converter: 'post_list_converter')]
class PostListController extends AbstractController
{
    public function __construct(
        private readonly PostListView $postListView,
        private readonly PostListUseCase $postListUseCase,
        private readonly PostListPresenter $presenter
    ) {
    }

    public function __invoke(PostListRequest $postListRequest): JsonResponse
    {
        $this->postListUseCase->execute($postListRequest, $this->presenter);

        return $this->postListView->generate($this->presenter->viewModel());
    }
}
