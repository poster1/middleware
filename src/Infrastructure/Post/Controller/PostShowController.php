<?php

namespace App\Infrastructure\Post\Controller;

use App\Infrastructure\Post\Presenter\PostShowPresenter;
use App\Infrastructure\Post\View\PostShowView;
use App\UseCase\Post\Show\PostShowRequest;
use App\UseCase\Post\Show\PostShowUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/post/{id}', name: 'post_show', methods: ['GET'])]
#[ParamConverter('postShowRequest', converter: 'post_show_converter')]
class PostShowController extends AbstractController
{
    public function __construct(
        private readonly PostShowView $postShowView,
        private readonly PostShowUseCase $postShowUseCase,
        private readonly PostShowPresenter $presenter
    ) {
    }

    public function __invoke(PostShowRequest $postShowRequest): JsonResponse
    {
        $this->postShowUseCase->execute($postShowRequest, $this->presenter);

        return $this->postShowView->generate($this->presenter->viewModel());
    }
}
