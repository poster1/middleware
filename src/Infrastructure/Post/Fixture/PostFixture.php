<?php

namespace App\Infrastructure\Post\Fixture;

use App\Domain\Post\Entity\Post;
use App\Infrastructure\User\Fixture\UserFixture;
use App\Infrastructure\User\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PostFixture extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $users = $this->userRepository->findAll();

        $persisted = 0;
        for ($i = 1; $i <= 10; ++$i) {
            $post = new Post();
            $post->setContent($faker->text());
            $post->setAuthor($faker->randomElement($users));

            $manager->persist($post);
            ++$persisted;
            if ($persisted >= 100) {
                $manager->flush();
            }
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [UserFixture::class];
    }
}
