<?php

namespace App\Infrastructure\Post\Presenter;

use App\Domain\Post\Interface\PostListPresenterInterface;
use App\Infrastructure\Post\ViewModel\PostListViewModel;
use App\UseCase\Post\List\PostListResponse;

class PostListPresenter implements PostListPresenterInterface
{
    private PostListViewModel $viewModel;

    public function present(PostListResponse $postListResponse): void
    {
        $this->viewModel = new PostListViewModel();
        $this->viewModel->posts = $postListResponse->getPosts();
        $this->viewModel->page = $postListResponse->getPage();
        $this->viewModel->itemPerPage = $postListResponse->getItemPerPage();
        $this->viewModel->violations = $postListResponse->getViolations();
    }

    public function viewModel(): PostListViewModel
    {
        return $this->viewModel;
    }
}
