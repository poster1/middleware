<?php

namespace App\Infrastructure\Post\Presenter;

use App\Domain\Post\Interface\PostShowPresenterInterface;
use App\Infrastructure\Post\ViewModel\PostShowViewModel;
use App\UseCase\Post\Show\PostShowResponse;

class PostShowPresenter implements PostShowPresenterInterface
{
    private PostShowViewModel $viewModel;

    public function present(PostShowResponse $postShowResponse): void
    {
        $this->viewModel = new PostShowViewModel();
        $this->viewModel->post = $postShowResponse->getPost();
        $this->viewModel->violations = $postShowResponse->getViolations();
    }

    public function viewModel(): PostShowViewModel
    {
        return $this->viewModel;
    }
}
