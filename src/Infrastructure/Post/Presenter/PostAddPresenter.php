<?php

namespace App\Infrastructure\Post\Presenter;

use App\Domain\Post\Interface\PostAddPresenterInterface;
use App\Infrastructure\Post\ViewModel\PostAddViewModel;
use App\UseCase\Post\Add\PostAddResponse;

class PostAddPresenter implements PostAddPresenterInterface
{
    private PostAddViewModel $viewModel;

    public function present(PostAddResponse $postAddResponse): void
    {
        $this->viewModel = new PostAddViewModel();
        $this->viewModel->post = $postAddResponse->getPost();
        $this->viewModel->violations = $postAddResponse->getViolations();
    }

    public function viewModel(): PostAddViewModel
    {
        return $this->viewModel;
    }
}
