<?php

namespace App\Infrastructure\Post\ParamConverter;

use App\Infrastructure\Post\Validator\PostListDataValidator;
use App\UseCase\Post\List\PostListRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class PostListConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly PostListDataValidator $postListDataValidator
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $postListData = json_decode($content, true, flags: JSON_THROW_ON_ERROR);

        $postListRequest = new PostListRequest();
        $postListRequest->violations = $this->postListDataValidator->gerErrors($postListData ?? null);
        $postListRequest->authorId = $postListData['authorId'] ?? null;
        $postListRequest->page = $postListData['page'] ?? 1;
        $postListRequest->itemPerPage = $postListData['itemPerPage'] ?? 10;

        $request->attributes->set($configuration->getName(), $postListRequest);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'postListRequest' === $configuration->getName() &&
            'post_list_converter' === $configuration->getConverter()
        ;
    }
}
