<?php

namespace App\Infrastructure\Post\ParamConverter;

use App\Domain\User\Entity\User;
use App\Infrastructure\Post\Validator\PostAddDataValidator;
use App\UseCase\Post\Add\PostAddRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

class PostAddConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly PostAddDataValidator $postAddDataValidator,
        private readonly Security $security,
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $postAddData = json_decode($content, true, flags: JSON_THROW_ON_ERROR);

        $postAddRequest = new PostAddRequest();
        $postAddRequest->violations = $this->postAddDataValidator->gerErrors($postAddData ?? null);
        $postAddRequest->isPosted = true;
        $postAddRequest->content = $postAddData['content'] ?? null;
        /** @var User $user */
        $user = $this->security->getUser();
        $postAddRequest->authorId = $postAddData['authorId'] ?? $user->getId();
        $postAddRequest->id = 0;

        $request->attributes->set($configuration->getName(), $postAddRequest);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'postAddRequest' === $configuration->getName() &&
            'post_add_converter' === $configuration->getConverter()
        ;
    }
}
