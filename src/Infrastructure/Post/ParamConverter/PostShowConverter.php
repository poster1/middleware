<?php

namespace App\Infrastructure\Post\ParamConverter;

use App\Infrastructure\Post\Validator\PostShowDataValidator;
use App\UseCase\Post\Show\PostShowRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class PostShowConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly PostShowDataValidator $postShowDataValidator
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $id = (int) $request->get('id', 0);
        $postShowData = ['id' => $id];

        $postShowRequest = new PostShowRequest();
        $postShowRequest->violations = $this->postShowDataValidator->gerErrors($postShowData ?? null);
        $postShowRequest->id = $id;

        $request->attributes->set($configuration->getName(), $postShowRequest);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'postShowRequest' === $configuration->getName() &&
            'post_show_converter' === $configuration->getConverter()
        ;
    }
}
