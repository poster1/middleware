<?php /** @noinspection PhpMultipleClassDeclarationsInspection */

namespace App\Infrastructure\Post\Repository;

use App\Domain\Post\Entity\Post;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\UseCase\Post\List\PostListRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Post>
 *
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post|null findOneByEmail(string $email)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository implements PostRepositoryInterface
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    public function __construct(
        private readonly ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Post::class);
    }

    public function save(Post $post, bool $flush = false): void
    {
        $this->getEntityManager()->persist($post);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function add(Post $post, bool $flush = false): void
    {
        $this->getEntityManager()->persist($post);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Post $post, bool $flush = false): void
    {
        $this->getEntityManager()->remove($post);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByPage(int $page, int $itemPerPage): array
    {
        return $this->createQueryBuilder('p')
            ->setFirstResult($itemPerPage * ($page - 1))
            ->setMaxResults($itemPerPage)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByAuthorId(int $authorId): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.author = :authorId')
            ->setParameter('authorId', $authorId)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countAll(): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findByPostListRequest(PostListRequest $postListRequest): array
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
        ;

        if ($postListRequest->authorId) {
            $queryBuilder
                ->andWhere('p.author = :authorId')
                ->setParameter('authorId', $postListRequest->authorId)
            ;
        }

        if ($postListRequest->page && $postListRequest->itemPerPage) {
            $queryBuilder
                ->setFirstResult($postListRequest->itemPerPage * ($postListRequest->page - 1))
                ->setMaxResults($postListRequest->itemPerPage)
            ;
        }

        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }
}
