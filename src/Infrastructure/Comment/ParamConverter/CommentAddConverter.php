<?php

namespace App\Infrastructure\Comment\ParamConverter;

use App\Domain\User\Entity\User;
use App\Infrastructure\Comment\Validator\CommentAddDataValidator;
use App\UseCase\Comment\Add\CommentAddRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

class CommentAddConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly CommentAddDataValidator $commentAddDataValidator,
        private readonly Security $security
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $commentAddData = json_decode($content, true, flags: JSON_THROW_ON_ERROR);

        $commentAddRequest = new CommentAddRequest();
        $commentAddRequest->violations = $this->commentAddDataValidator->gerErrors($commentAddData ?? null);
        $commentAddRequest->isPosted = true;
        $commentAddRequest->content = $commentAddData['content'] ?? null;
        $commentAddRequest->postId = $commentAddData['postId'] ?? null;
        /** @var User $user */
        $user = $this->security->getUser();
        $commentAddRequest->authorId = $commentAddData['authorId'] ?? $user->getId();
        $commentAddRequest->parentId = $commentAddData['parentId'] ?? null;
        $commentAddRequest->id = 0;

        $request->attributes->set($configuration->getName(), $commentAddRequest);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'commentAddRequest' === $configuration->getName() &&
            'comment_add_converter' === $configuration->getConverter()
        ;
    }
}
