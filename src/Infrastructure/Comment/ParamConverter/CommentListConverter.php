<?php

namespace App\Infrastructure\Comment\ParamConverter;

use App\Infrastructure\Comment\Validator\CommentListDataValidator;
use App\UseCase\Comment\List\CommentListRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class CommentListConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly CommentListDataValidator $commentListDataValidator
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $commentListData = json_decode($content, true, flags: JSON_THROW_ON_ERROR);

        $commentListRequest = new CommentListRequest();
        $commentListRequest->violations = $this->commentListDataValidator->gerErrors($commentListData ?? null);
        $commentListRequest->postId = $commentListData['postId'] ?? null;
        $commentListRequest->authorId = $commentListData['authorId'] ?? null;
        $commentListRequest->parentId = $commentListData['parentId'] ?? null;
        $commentListRequest->page = $commentListData['page'] ?? 1;
        $commentListRequest->itemPerPage = $commentListData['itemPerPage'] ?? 10;

        $request->attributes->set($configuration->getName(), $commentListRequest);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'commentListRequest' === $configuration->getName() &&
            'comment_list_converter' === $configuration->getConverter()
        ;
    }
}
