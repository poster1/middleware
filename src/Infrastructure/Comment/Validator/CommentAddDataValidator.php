<?php

namespace App\Infrastructure\Comment\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CommentAddDataValidator
{
    private Assert\Collection $constraint;

    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
        $this->constraint = new Assert\Collection([
            'content' => new Assert\Required([
                new Assert\NotBlank(),
                new Assert\Length(null, 1, 500),
            ]),
            'postId' => new Assert\Required([
                new Assert\Type('int'),
            ]),
            'authorId' => new Assert\Optional([
                new Assert\Type('int'),
            ]),
            'parentId' => new Assert\Optional([
                new Assert\Type('int'),
            ]),
            'isPosted' => new Assert\Optional([
                new Assert\Type('string'),
            ]),
            'save' => new Assert\Optional(),
        ]);
    }

    public function gerErrors(?array $registerUserData): ?array
    {
        $violations = $this->validator->validate($registerUserData, $this->constraint);
        if (0 === \count($violations)) {
            return null;
        }

        $messages = [];
        foreach ($violations as $violation) {
            $messages[] = [
                $violation->getPropertyPath() => $violation->getMessage(),
            ];
        }

        return $messages;
    }
}
