<?php

namespace App\Infrastructure\Comment\Presenter;

use App\Domain\Comment\Interface\CommentListPresenterInterface;
use App\Infrastructure\Comment\ViewModel\CommentListViewModel;
use App\UseCase\Comment\List\CommentListResponse;

class CommentListPresenter implements CommentListPresenterInterface
{
    private CommentListViewModel $viewModel;

    public function present(CommentListResponse $commentListResponse): void
    {
        $this->viewModel = new CommentListViewModel();
        $this->viewModel->comments = $commentListResponse->getComments();
        $this->viewModel->page = $commentListResponse->getPage();
        $this->viewModel->itemPerPage = $commentListResponse->getItemPerPage();
        $this->viewModel->violations = $commentListResponse->getViolations();
    }

    public function viewModel(): CommentListViewModel
    {
        return $this->viewModel;
    }
}
