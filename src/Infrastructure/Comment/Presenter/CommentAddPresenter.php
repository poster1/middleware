<?php

namespace App\Infrastructure\Comment\Presenter;

use App\Domain\Comment\Interface\CommentAddPresenterInterface;
use App\Infrastructure\Comment\ViewModel\CommentAddViewModel;
use App\UseCase\Comment\Add\CommentAddResponse;

class CommentAddPresenter implements CommentAddPresenterInterface
{
    private CommentAddViewModel $viewModel;

    public function present(CommentAddResponse $commentAddResponse): void
    {
        $this->viewModel = new CommentAddViewModel();
        $this->viewModel->comment = $commentAddResponse->getComment();
        $this->viewModel->violations = $commentAddResponse->getViolations();
    }

    public function viewModel(): CommentAddViewModel
    {
        return $this->viewModel;
    }
}
