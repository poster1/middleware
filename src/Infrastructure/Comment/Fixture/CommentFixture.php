<?php

namespace App\Infrastructure\Comment\Fixture;

use App\Domain\Comment\Entity\Comment;
use App\Infrastructure\Post\Fixture\PostFixture;
use App\Infrastructure\Post\Repository\PostRepository;
use App\Infrastructure\User\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CommentFixture extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly PostRepository $postRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $users = $this->userRepository->findAll();
        $posts = $this->postRepository->findAll();
        $comments = [];

        $persisted = 0;
        for ($i = 1; $i <= 100; ++$i) {
            $comment = new Comment();
            $comment->setContent($faker->text(500));
            $comment->setPost($faker->randomElement($posts));
            $comment->setAuthor($faker->randomElement($users));
            $comment->setParent($faker->randomElement($comments));

            $manager->persist($comment);
            ++$persisted;
            if ($persisted >= 100) {
                $manager->flush();
            }
            $comments[] = $comment;
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [PostFixture::class];
    }
}
