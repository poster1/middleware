<?php /** @noinspection PhpMultipleClassDeclarationsInspection */

namespace App\Infrastructure\Comment\Repository;

use App\Domain\Comment\Repository\CommentRepositoryInterface;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\Comment\Entity\Comment;
use App\UseCase\Comment\List\CommentListRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Comment>
 *
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment|null findOneByEmail(string $email)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository implements CommentRepositoryInterface
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    public function __construct(
        private readonly ManagerRegistry $registry,
        private readonly UserRepositoryInterface $userRepository,
        private readonly PostRepositoryInterface $postRepository,
    ) {
        parent::__construct($registry, Comment::class);
    }

    public function save(Comment $comment, bool $flush = false): void
    {
        $this->getEntityManager()->persist($comment);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function add(Comment $comment, bool $flush = false): void
    {
        $this->getEntityManager()->persist($comment);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Comment $comment, bool $flush = false): void
    {
        $this->getEntityManager()->remove($comment);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByPostId(int $postId): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.postId = :postId')
            ->setParameter('postId', $postId)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByAuthorId(int $authorId): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.postId = :authorId')
            ->setParameter('authorId', $authorId)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByParentId(int $parentId): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.parentId = :parentId')
            ->setParameter('parentId', $parentId)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByCommentListRequest(CommentListRequest $commentListRequest): array
    {
        $queryBuilder = $this->createQueryBuilder('c');

        if ($commentListRequest->postId) {
            $queryBuilder->andWhere('c.postId = :postId');
            $queryBuilder->setParameter('postId', $commentListRequest->postId);
        }
        if ($commentListRequest->authorId) {
            $queryBuilder->andWhere('c.authorId = :authorId');
            $queryBuilder->setParameter('authorId', $commentListRequest->authorId);
        }
        if ($commentListRequest->page and $commentListRequest->itemPerPage) {
            $queryBuilder->setFirstResult($commentListRequest->itemPerPage * ($commentListRequest->page - 1));
            $queryBuilder->setMaxResults($commentListRequest->itemPerPage);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
