<?php

namespace App\Infrastructure\Comment\Controller;

use App\Infrastructure\Comment\Presenter\CommentListPresenter;
use App\Infrastructure\Comment\View\CommentListView;
use App\UseCase\Comment\List\CommentListRequest;
use App\UseCase\Comment\List\CommentListUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/comment', name: 'comment_list', methods: ['GET'])]
#[ParamConverter('commentListRequest', converter: 'comment_list_converter')]
class CommentListController extends AbstractController
{
    public function __construct(
        private readonly CommentListView $commentListView,
        private readonly CommentListUseCase $commentListUseCase,
        private readonly CommentListPresenter $presenter
    ) {
    }

    public function __invoke(CommentListRequest $commentListRequest): JsonResponse
    {
        $this->commentListUseCase->execute($commentListRequest, $this->presenter);

        return $this->commentListView->generate($this->presenter->viewModel());
    }
}
