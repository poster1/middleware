<?php

namespace App\Infrastructure\Comment\Controller;

use App\Infrastructure\Comment\Presenter\CommentAddPresenter;
use App\Infrastructure\Comment\View\CommentAddView;
use App\UseCase\Comment\Add\CommentAddRequest;
use App\UseCase\Comment\Add\CommentAddUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/comment', name: 'comment_add', methods: ['POST'])]
#[ParamConverter('commentAddRequest', converter: 'comment_add_converter')]
class CommentAddController extends AbstractController
{
    public function __construct(
        private readonly CommentAddView $commentAddView,
        private readonly CommentAddUseCase $commentAddUseCase,
        private readonly CommentAddPresenter $presenter
    ) {
    }

    public function __invoke(CommentAddRequest $commentAddRequest): JsonResponse
    {
        $this->commentAddUseCase->execute($commentAddRequest, $this->presenter);

        return $this->commentAddView->generate($this->presenter->viewModel());
    }
}
