<?php

namespace App\Infrastructure\Comment\ViewModel;

class CommentListViewModel
{
    public ?array $comments;
    public ?int $page;
    public ?int $itemPerPage;

    public ?array $violations;
}
