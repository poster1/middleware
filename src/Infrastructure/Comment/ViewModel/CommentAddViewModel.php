<?php

namespace App\Infrastructure\Comment\ViewModel;

use App\Domain\Comment\Entity\Comment;

class CommentAddViewModel
{
    public ?Comment $comment;
    public ?array $violations;
}
