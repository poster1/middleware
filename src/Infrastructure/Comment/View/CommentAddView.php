<?php

namespace App\Infrastructure\Comment\View;

use App\Domain\Comment\Entity\Comment;
use App\Infrastructure\Comment\ViewModel\CommentAddViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentAddView
{
    public function generate(
        CommentAddViewModel $commentAddViewModel
    ): JsonResponse {
        if ($commentAddViewModel->violations) {
            return new JsonResponse([['errors' => $commentAddViewModel->violations], 400]);
        }

        return new JsonResponse(
            [
                'data' => Comment::toArray($commentAddViewModel->comment),
            ]
        );
    }
}
