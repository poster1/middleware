<?php

namespace App\Infrastructure\Comment\View;

use App\Domain\Comment\Entity\Comment;
use App\Infrastructure\Comment\ViewModel\CommentListViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentListView
{
    public function generate(
        CommentListViewModel $commentListViewModel
    ): JsonResponse {
        if ($commentListViewModel->violations) {
            return new JsonResponse(['errors' => $commentListViewModel->violations], 400);
        }

        return new JsonResponse(
            [
                'data' => array_map(
                    fn (Comment $comment) => Comment::toArray($comment),
                    $commentListViewModel->comments
                ),
                'page' => $commentListViewModel->page,
                'itemPerPage' => $commentListViewModel->itemPerPage,
            ]
        );
    }
}
