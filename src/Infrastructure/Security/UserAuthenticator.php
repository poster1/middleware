<?php

namespace App\Infrastructure\Security;

use App\Domain\User\Entity\User;
use App\Infrastructure\User\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use function Symfony\Component\String\u;

class UserAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    public function supports(Request $request): ?bool
    {
        $auth = $request->headers->get('Authorization');

        return u($auth)->startsWith('Bearer');
    }

    public function authenticate(Request $request): Passport
    {
        $token = u($request->headers->get('Authorization'))->replace('Bearer ', '')->replace('Bearer', '')->toString();

        if (!$token) {
            throw new CustomUserMessageAuthenticationException('no_api_token_provided');
        }

        return new Passport(
            new UserBadge($token, function ($userIdentifier) {
                /** @var User $user */
                $user = $this->userRepository->findOneByToken($userIdentifier);

                if (!$user) {
                    throw new UserNotFoundException();
                }

//                if ($user->getTokenExpiredAt() && $user->getTokenExpiredAt() < new \DateTime()) {
//                    throw new CustomUserMessageAuthenticationException('token_is_expired');
//                }

                return $user;
            }),
            new CustomCredentials(
                /* @var User $user */
                fn ($credentials, UserInterface $user) => $user->getToken() === $credentials,
                $token
            )
        );
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new JsonResponse(['message' => 'auth_header_required'], Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }
}
