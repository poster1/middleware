<?php

namespace App\Infrastructure\User\View;

use App\Domain\User\Entity\User;
use App\Infrastructure\User\ViewModel\UserLoginViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserLoginView
{
    public function generate(
        UserLoginViewModel $viewModel
    ): JsonResponse {
        if ($viewModel->violations) {
            return new JsonResponse(['errors' => $viewModel->violations], 400);
        }

        return new JsonResponse(
            ['data' => [
                'token' => $viewModel->token,
                'user' => User::toArray($viewModel->user),
            ]]
        );
    }
}
