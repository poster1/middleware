<?php

namespace App\Infrastructure\User\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterUserDataValidator
{
    private Assert\Collection $constraint;

    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
        $this->constraint = new Assert\Collection([
            'email' => new Assert\Required([
                new Assert\Email(),
            ]),
            'isPosted' => new Assert\Optional([
                new Assert\Type('string'),
            ]),
            'password' => new Assert\Required([
                new Assert\NotBlank(),
                new Assert\Length(null, 8, 50),
            ]),
            'firstName' => new Assert\Required([
                new Assert\NotBlank(),
            ]),
            'lastName' => new Assert\Required([
                new Assert\NotBlank(),
            ]),
            'save' => new Assert\Optional(),
        ]);
    }

    public function gerErrors(?array $registerUserData): ?array
    {
        $violations = $this->validator->validate($registerUserData, $this->constraint);
        if (0 === \count($violations)) {
            return null;
        }

        $messages = [];
        foreach ($violations as $violation) {
            $messages[] = [
                $violation->getPropertyPath() => $violation->getMessage(),
            ];
        }

        return $messages;
    }
}
