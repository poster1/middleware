<?php

namespace App\Infrastructure\User\ViewModel;

use App\Domain\User\Entity\User;

class UserLoginViewModel
{
    public ?string $token;
    public ?User $user;
    public ?array $violations;
}
