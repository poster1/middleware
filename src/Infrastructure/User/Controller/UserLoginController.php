<?php

namespace App\Infrastructure\User\Controller;

use App\Infrastructure\User\Presenter\UserLoginPresenter;
use App\Infrastructure\User\View\UserLoginView;
use App\UseCase\User\Login\UserLoginRequest;
use App\UseCase\User\Login\UserLoginUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user/login', name: 'user_login')]
#[ParamConverter('userLoginRequest', converter: 'user_login_converter')]
class UserLoginController extends AbstractController
{
    public function __construct(
        private readonly UserLoginView $view,
        private readonly UserLoginUseCase $useCase,
        private readonly UserLoginPresenter $presenter
    ) {
    }

    public function __invoke(UserLoginRequest $userLoginRequest): JsonResponse
    {
        $this->useCase->execute($userLoginRequest, $this->presenter);

        return $this->view->generate($this->presenter->viewModel());
    }
}
