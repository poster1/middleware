<?php

namespace App\Infrastructure\User\Controller;

use App\Infrastructure\User\Presenter\RegisterUserPresenter;
use App\Infrastructure\User\View\RegisterUserView;
use App\UseCase\User\Register\RegisterUserRequest;
use App\UseCase\User\Register\RegisterUserUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user/register', name: 'user_register')]
#[ParamConverter('registerUserRequest', converter: 'register_user_converter')]
class RegisterUserController extends AbstractController
{
    public function __construct(
        private readonly RegisterUserView $view,
        private readonly RegisterUserUseCase $useCase,
        private readonly RegisterUserPresenter $presenter
    ) {
    }

    public function __invoke(RegisterUserRequest $registerUserRequest): JsonResponse
    {
        $this->useCase->execute($registerUserRequest, $this->presenter);

        return $this->view->generate($this->presenter->viewModel());
    }
}
