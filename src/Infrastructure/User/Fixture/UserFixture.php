<?php

namespace App\Infrastructure\User\Fixture;

use App\Domain\User\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixture extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $persisted = 0;
        for ($i = 1; $i <= 10; ++$i) {
            $user = new User();
            $user->setFirstName($faker->firstName());
            $user->setLastName($faker->lastName());
            $user->setEmail("user-$i@company.com");
            $user->setPassword($this->userPasswordHasher->hashPassword($user, 'password'));

            $manager->persist($user);
            ++$persisted;
            if ($persisted >= 100) {
                $manager->flush();
            }
        }

        $manager->flush();
    }
}
