<?php

namespace App\Infrastructure\User\Presenter;

use App\Domain\User\Interface\UserLoginPresenterInterface;
use App\Infrastructure\User\ViewModel\UserLoginViewModel;
use App\UseCase\User\Login\UserLoginResponse;

class UserLoginPresenter implements UserLoginPresenterInterface
{
    private UserLoginViewModel $userLoginViewModel;

    public function present(UserLoginResponse $userLoginResponse): void
    {
        $this->userLoginViewModel = new UserLoginViewModel();
        $this->userLoginViewModel->token = $userLoginResponse->getToken();
        $this->userLoginViewModel->user = $userLoginResponse->getUser();
        $this->userLoginViewModel->violations = $userLoginResponse->getViolations();
    }

    public function viewModel(): UserLoginViewModel
    {
        return $this->userLoginViewModel;
    }
}
