<?php

namespace App\Infrastructure\User\Presenter;

use App\Domain\User\Interface\RegisterUserPresenterInterface;
use App\Infrastructure\User\ViewModel\RegisterUserViewModel;
use App\UseCase\User\Register\RegisterUserResponse;

class RegisterUserPresenter implements RegisterUserPresenterInterface
{
    private RegisterUserViewModel $viewModel;

    public function present(RegisterUserResponse $response): void
    {
        $this->viewModel = new RegisterUserViewModel();
        $this->viewModel->user = $response->getUser();
        $this->viewModel->token = $response->getUser()?->getToken() ?? null;
        $this->viewModel->violations = $response->getViolations();
    }

    public function viewModel(): RegisterUserViewModel
    {
        return $this->viewModel;
    }
}
