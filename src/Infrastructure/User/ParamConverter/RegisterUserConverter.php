<?php

namespace App\Infrastructure\User\ParamConverter;

use App\Infrastructure\User\Validator\RegisterUserDataValidator;
use App\UseCase\User\Register\RegisterUserRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class RegisterUserConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly RegisterUserDataValidator $registerUserDataValidator
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $registerUserData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $registerUserRequestUseCase = new RegisterUserRequest();
        $violations = $this->registerUserDataValidator->gerErrors($registerUserData ?? null);
        $registerUserRequestUseCase->violations = $violations;
        $registerUserRequestUseCase->isPosted = true;
        $registerUserRequestUseCase->email = $registerUserData['email'] ?? null;
        $registerUserRequestUseCase->password = $registerUserData['password'] ?? null;
        $registerUserRequestUseCase->firstName = $registerUserData['firstName'] ?? null;
        $registerUserRequestUseCase->lastName = $registerUserData['lastName'] ?? null;
        $registerUserRequestUseCase->id = 0;

        $request->attributes->set($configuration->getName(), $registerUserRequestUseCase);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'registerUserRequest' === $configuration->getName() &&
            'register_user_converter' === $configuration->getConverter()
        ;
    }
}
