<?php

namespace App\Infrastructure\User\ParamConverter;

use App\Infrastructure\User\Validator\UserLoginDataValidator;
use App\UseCase\User\Login\UserLoginRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class UserLoginConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly UserLoginDataValidator $userLoginDataValidator
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $userLoginData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $userLoginRequestUseCase = new UserLoginRequest();
        $userLoginRequestUseCase->violations = $this->userLoginDataValidator->gerErrors($userLoginData ?? null);
        $userLoginRequestUseCase->isPosted = true;
        $userLoginRequestUseCase->email = $userLoginData['email'] ?? null;
        $userLoginRequestUseCase->password = $userLoginData['password'] ?? null;

        $request->attributes->set($configuration->getName(), $userLoginRequestUseCase);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'userLoginRequest' === $configuration->getName() &&
            'user_login_converter' === $configuration->getConverter()
        ;
    }
}
