<?php

namespace App\Infrastructure\Rating\Controller;

use App\Infrastructure\Rating\Presenter\RatingAddPresenter;
use App\Infrastructure\Rating\View\RatingAddView;
use App\UseCase\Rating\Add\RatingAddRequest;
use App\UseCase\Rating\Add\RatingAddUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/rating', name: 'rating_add', methods: ['POST'])]
#[ParamConverter('ratingAddRequest', converter: 'rating_add_converter')]
class RatingAddController extends AbstractController
{
    public function __construct(
        private readonly RatingAddView $ratingAddView,
        private readonly RatingAddUseCase $ratingAddUseCase,
        private readonly RatingAddPresenter $presenter
    ) {
    }

    public function __invoke(RatingAddRequest $ratingAddRequest): JsonResponse
    {
        $this->ratingAddUseCase->execute($ratingAddRequest, $this->presenter);

        return $this->ratingAddView->generate($this->presenter->viewModel());
    }
}
