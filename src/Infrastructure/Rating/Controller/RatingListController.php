<?php

namespace App\Infrastructure\Rating\Controller;

use App\Infrastructure\Rating\Presenter\RatingListPresenter;
use App\Infrastructure\Rating\View\RatingListView;
use App\UseCase\Rating\List\RatingListRequest;
use App\UseCase\Rating\List\RatingListUseCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/rating', name: 'rating_list', methods: ['GET'])]
#[ParamConverter('ratingListRequest', converter: 'rating_list_converter')]
class RatingListController extends AbstractController
{
    public function __construct(
        private readonly RatingListView $ratingListView,
        private readonly RatingListUseCase $ratingListUseCase,
        private readonly RatingListPresenter $presenter
    ) {
    }

    public function __invoke(RatingListRequest $ratingListRequest): JsonResponse
    {
        $this->ratingListUseCase->execute($ratingListRequest, $this->presenter);

        return $this->ratingListView->generate($this->presenter->viewModel());
    }
}
