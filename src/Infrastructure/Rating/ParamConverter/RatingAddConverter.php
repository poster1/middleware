<?php

namespace App\Infrastructure\Rating\ParamConverter;

use App\Domain\User\Entity\User;
use App\Infrastructure\Rating\Validator\RatingAddDataValidator;
use App\UseCase\Rating\Add\RatingAddRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

class RatingAddConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly RatingAddDataValidator $ratingAddDataValidator,
        private readonly Security $security,
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $ratingAddData = json_decode($content, true, flags: JSON_THROW_ON_ERROR);

        $ratingAddRequest = new RatingAddRequest();
        $ratingAddRequest->violations = $this->ratingAddDataValidator->gerErrors($ratingAddData ?? null);
        $ratingAddRequest->isPosted = true;
        $ratingAddRequest->value = $ratingAddData['value'] ?? null;
        $ratingAddRequest->commentId = $ratingAddData['commentId'] ?? null;
        /** @var User $user */
        $user = $this->security->getUser();
        $ratingAddRequest->authorId = $ratingAddData['authorId'] ?? $user->getId();
        $ratingAddRequest->id = 0;

        $request->attributes->set($configuration->getName(), $ratingAddRequest);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'ratingAddRequest' === $configuration->getName() &&
            'rating_add_converter' === $configuration->getConverter()
        ;
    }
}
