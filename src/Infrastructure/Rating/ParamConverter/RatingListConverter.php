<?php

namespace App\Infrastructure\Rating\ParamConverter;

use App\Infrastructure\Rating\Validator\RatingListDataValidator;
use App\UseCase\Rating\List\RatingListRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class RatingListConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly RatingListDataValidator $ratingListDataValidator
    ) {
    }

    /**
     * @throws \JsonException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $content = $request->getContent() ? $request->getContent() : '[]';
        $ratingListData = json_decode($content, true, flags: JSON_THROW_ON_ERROR);

        $ratingListRequest = new RatingListRequest();
        $ratingListRequest->violations = $this->ratingListDataValidator->gerErrors($ratingListData ?? null);
        $ratingListRequest->commentId = $ratingListData['commentId'] ?? null;
        $ratingListRequest->authorId = $ratingListData['authorId'] ?? null;
        $ratingListRequest->page = $ratingListData['page'] ?? 1;
        $ratingListRequest->itemPerPage = $ratingListData['itemPerPage'] ?? 10;

        $request->attributes->set($configuration->getName(), $ratingListRequest);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return
            'ratingListRequest' === $configuration->getName() &&
            'rating_list_converter' === $configuration->getConverter()
        ;
    }
}
