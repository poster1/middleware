<?php /** @noinspection PhpMultipleClassDeclarationsInspection */

namespace App\Infrastructure\Rating\Repository;

use App\Domain\Rating\Entity\Rating;
use App\Domain\Rating\Repository\RatingRepositoryInterface;
use App\UseCase\Rating\List\RatingListRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Rating>
 *
 * @method Rating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rating|null findOneByEmail(string $email)
 * @method Rating[]    findAll()
 * @method Rating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingRepository extends ServiceEntityRepository implements RatingRepositoryInterface
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    public function __construct(
        private readonly ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Rating::class);
    }

    public function save(Rating $rating, bool $flush = false): void
    {
        $this->getEntityManager()->persist($rating);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function add(Rating $rating, bool $flush = false): void
    {
        $this->getEntityManager()->persist($rating);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Rating $rating, bool $flush = false): void
    {
        $this->getEntityManager()->remove($rating);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByRatingListRequest(RatingListRequest $ratingListRequest): array
    {
        $queryBuilder = $this->createQueryBuilder('r');

        if ($ratingListRequest->commentId) {
            $queryBuilder->andWhere('c.commentId = :commentId');
            $queryBuilder->setParameter('commentId', $ratingListRequest->commentId);
        }
        if ($ratingListRequest->authorId) {
            $queryBuilder->andWhere('c.authorId = :authorId');
            $queryBuilder->setParameter('authorId', $ratingListRequest->authorId);
        }
        if ($ratingListRequest->page and $ratingListRequest->itemPerPage) {
            $queryBuilder->setFirstResult($ratingListRequest->itemPerPage * ($ratingListRequest->page - 1));
            $queryBuilder->setMaxResults($ratingListRequest->itemPerPage);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
