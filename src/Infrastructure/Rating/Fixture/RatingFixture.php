<?php

namespace App\Infrastructure\Rating\Fixture;

use App\Domain\Rating\Entity\Rating;
use App\Infrastructure\Comment\Fixture\CommentFixture;
use App\Infrastructure\Comment\Repository\CommentRepository;
use App\Infrastructure\User\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RatingFixture extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly CommentRepository $commentRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $users = $this->userRepository->findAll();
        $comments = $this->commentRepository->findAll();

        $persisted = 0;
        for ($i = 1; $i <= 10; ++$i) {
            $post = new Rating();
            $post->setValue($faker->numberBetween(0, 5));
            $post->setAuthor($faker->randomElement($users));
            $post->setComment($faker->randomElement($comments));

            $manager->persist($post);
            ++$persisted;
            if ($persisted >= 100) {
                $manager->flush();
            }
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [CommentFixture::class];
    }
}
