<?php

namespace App\Infrastructure\Rating\View;

use App\Domain\Rating\Entity\Rating;
use App\Infrastructure\Rating\Factory\RatingToArrayFactory;
use App\Infrastructure\Rating\ViewModel\RatingListViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class RatingListView
{
    public function generate(
        RatingListViewModel $ratingListViewModel
    ): JsonResponse {
        if ($ratingListViewModel->violations) {
            return new JsonResponse(['errors' => $ratingListViewModel->violations], 400);
        }

        return new JsonResponse(
            [
                'data' => array_map(fn (Rating $rating) => Rating::toArray($rating), $ratingListViewModel->ratings),
                'page' => $ratingListViewModel->page,
                'itemPerPage' => $ratingListViewModel->itemPerPage,
            ]
        );
    }
}
