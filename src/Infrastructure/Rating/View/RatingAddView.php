<?php

namespace App\Infrastructure\Rating\View;

use App\Domain\Rating\Entity\Rating;
use App\Infrastructure\Rating\ViewModel\RatingAddViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class RatingAddView
{
    public function generate(
        RatingAddViewModel $ratingAddViewModel
    ): JsonResponse {
        if ($ratingAddViewModel->violations) {
            return new JsonResponse(['errors' => $ratingAddViewModel->violations], 400);
        }

        return new JsonResponse(
            [
                'data' => Rating::toArray($ratingAddViewModel->rating),
            ]
        );
    }
}
