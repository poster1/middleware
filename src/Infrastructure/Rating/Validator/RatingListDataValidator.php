<?php

namespace App\Infrastructure\Rating\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RatingListDataValidator
{
    private Assert\Collection $constraint;

    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
        $this->constraint = new Assert\Collection([
            'commentId' => new Assert\Optional([
                new Assert\Type('int'),
            ]),
            'authorId' => new Assert\Optional([
                new Assert\Type('int'),
            ]),
            'page' => new Assert\Optional([
                new Assert\Type('int'),
            ]),
            'itemPerPage' => new Assert\Optional([
                new Assert\Type('int'),
            ]),
        ]);
    }

    public function gerErrors(?array $registerUserData): ?array
    {
        $violations = $this->validator->validate($registerUserData, $this->constraint);
        if (0 === \count($violations)) {
            return null;
        }

        $messages = [];
        foreach ($violations as $violation) {
            $messages[] = [
                $violation->getPropertyPath() => $violation->getMessage(),
            ];
        }

        return $messages;
    }
}
