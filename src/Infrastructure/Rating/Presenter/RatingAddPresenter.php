<?php

namespace App\Infrastructure\Rating\Presenter;

use App\Domain\Rating\Interface\RatingAddPresenterInterface;
use App\Infrastructure\Rating\ViewModel\RatingAddViewModel;
use App\UseCase\Rating\Add\RatingAddResponse;

class RatingAddPresenter implements RatingAddPresenterInterface
{
    private RatingAddViewModel $viewModel;

    public function present(RatingAddResponse $ratingAddResponse): void
    {
        $this->viewModel = new RatingAddViewModel();
        $this->viewModel->rating = $ratingAddResponse->getRating();
        $this->viewModel->violations = $ratingAddResponse->getViolations();
    }

    public function viewModel(): RatingAddViewModel
    {
        return $this->viewModel;
    }
}
