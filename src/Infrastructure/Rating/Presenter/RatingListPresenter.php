<?php

namespace App\Infrastructure\Rating\Presenter;

use App\Domain\Rating\Interface\RatingListPresenterInterface;
use App\Infrastructure\Rating\ViewModel\RatingListViewModel;
use App\UseCase\Rating\List\RatingListResponse;

class RatingListPresenter implements RatingListPresenterInterface
{
    private RatingListViewModel $viewModel;

    public function present(RatingListResponse $ratingListResponse): void
    {
        $this->viewModel = new RatingListViewModel();
        $this->viewModel->ratings = $ratingListResponse->getRatings();
        $this->viewModel->page = $ratingListResponse->getPage();
        $this->viewModel->itemPerPage = $ratingListResponse->getItemPerPage();
        $this->viewModel->violations = $ratingListResponse->getViolations();
    }

    public function viewModel(): RatingListViewModel
    {
        return $this->viewModel;
    }
}
