<?php

namespace App\Infrastructure\Rating\ViewModel;

class RatingListViewModel
{
    public ?array $ratings;
    public ?int $page;
    public ?int $itemPerPage;

    public ?array $violations;
}
