<?php

namespace App\Infrastructure\Rating\ViewModel;

use App\Domain\Rating\Entity\Rating;

class RatingAddViewModel
{
    public ?Rating $rating;
    public ?array $violations;
}
