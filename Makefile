CURRENT_DIRECTORY := $(shell pwd)

TESTSCOPE = apps
TESTFLAGS = --with-timer --timer-top-n 10 --keepdb
TEST_FLAG = TRUE


cyan = echo -e "\x1b[36m\#\# $1\x1b[0m"

export COMPOSE_INTERACTIVE_NO_CLI=1

help:

	@echo "Available commands:"
	@echo "-----------------------"
	@echo ""
	@echo "Download and install dependencies:"
	@echo "    make vendor"
	@echo ""
	@echo "Clean up the prokect:"
	@echo "    make clean"
	@echo ""
	@echo "Drop an recreate database:"
	@echo "    make db"
	@echo ""
	@echo "Populate database with fake data:"
	@echo "    make data"
	@echo ""
	@echo "Start the app:"
	@echo "    make start"
	@echo ""
	@echo "Run all commands:"
	@echo "    make setup"
	@echo ""

clean:
	@$(call cyan, "cleaning up...")
	rm -rf var/cache var/log vendor composer.lock .php_cs.cache .phpcs-cache

vendor:
	@$(call cyan, "installing dependencies...") \
	symfony composer install

db:
	@$(call cyan, "dropping the database...") \
	symfony console doctrine:database:drop --force ; \
	@$(call cyan, "creating the database...") \
	symfony console doctrine:database:create ; \
	@$(call cyan, "creating the database schema...") \
	symfony console doctrine:schema:update --force

data:
	@$(call cyan, "loading fixtures...") \
	symfony console doctrine:fixtures:load -n

start:
	@$(call cyan, "serving the app...") \
	symfony serve -d --no-tls

setup:
	make clean \
	make vendor \
	make db \
	make data \
	make start